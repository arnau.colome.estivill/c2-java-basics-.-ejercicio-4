package ejercicio4;

public class ejercicio4 {

	public static void main(String[] args) {
		/*
		 4 - Escribe un programa Java que declare una variable entera N y as�gnale un valor. A
		continuaci�n escribe las instrucciones que realicen los siguientes:
		� Incrementar N en 77.
		� Decrementarla en 3.
		� Duplicar su valor.
		
		Si por ejemplo N = 1 la salida del programa ser�:
		� Valor inicial de N = 1
		� N + 77 = 78
		� N - 3 = 75
		� N * 2 = 150
		 */
		
		int N = 32;
		int resultado = 0;
		
		resultado = N;
		
		System.out.println("Valor incial de N: " + resultado);
		
		resultado = N + 77;
		System.out.println("N + 77: " + resultado);
		
		resultado = resultado - 3;
		System.out.println("N - 3: " + resultado);
		
		resultado = resultado * 2;
		System.out.println("N * 2: " + resultado);
		
	}

}
